Before launching this container, you must create an app and generate token visiting the following URL (you should be logged in):

https://www.dropbox.com/developers/apps

Then, clic on "generate token", get it and put it on your $HOME:
cat ~/.dropbox_uploader
OAUTH_ACCESS_TOKEN=XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

Share this file with the container using (docker-compose.yml example):
    volumes:
      - "/<path>/.dropbox_uploader:/root/.dropbox_uploader:ro"
 
