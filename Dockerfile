FROM  debian:jessie

# Set the timezone.
RUN echo "Europe/Paris" > /etc/timezone
RUN dpkg-reconfigure -f noninteractive tzdata

# Install cron
RUN apt-get update && apt-get -y install cron curl mariadb-client-10.0 && apt-get clean && rm -rf /var/lib/apt/lists/*

# Add crontab file in the cron directory
ADD crontab /etc/cron.d/backup_crontab

# Add shell script and grant execution rights
ADD backup_wordpress.sh /backup_wordpress.sh
RUN chmod +x /backup_wordpress.sh

# Give execution rights on the cron job
RUN chmod 0644 /etc/cron.d/backup_crontab

# Dropbox uploader part
RUN curl "https://raw.githubusercontent.com/andreafabrizi/Dropbox-Uploader/master/dropbox_uploader.sh" -o /dropbox_uploader.sh && chmod +x dropbox_uploader.sh

# Run the command on container startup
CMD ["cron", "-f" ]
