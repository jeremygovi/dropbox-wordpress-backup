#!/bin/bash
#set -x
 
# Necessite dropbox uploader:
# https://github.com/andreafabrizi/Dropbox-Uploader
 
NB_DAYS_TO_KEEP=10
TODAY=`date +"%Y-%m-%d"`
BACKUP_PATH="/backup"
TODAY_BACKUP_PATH="${BACKUP_PATH}/${TODAY}"
SITES_PATH="/var/www"
DROPBOX_BACKUP_SCRIPT="/dropbox_uploader.sh"
DROPBOX_BACKUP_PATH="/backup"
TODAY_DROPBOX_BACKUP_PATH="${DROPBOX_BACKUP_PATH}/${TODAY}"
 
function printWithDate() {
        echo "$(date +"[%Y-%m-%d %T]") $1 "
}
function log() {
        printWithDate "INFO: $1"
}
 
# Creation du répertoire de backup du jour
mkdir -p ${TODAY_BACKUP_PATH}
 
for SITE_NAME in `ls $SITES_PATH`
do
    if [ -f ${SITES_PATH}/${SITE_NAME}/wp-config.php ]
    then
    	log "Backuping database to ${TODAY_BACKUP_PATH}/db.${SITE_NAME}.sql"
    	DB_HOST=`cat ${SITES_PATH}/${SITE_NAME}/wp-config.php | grep -v "*" | grep -v "#" | grep define | grep DB_HOST | awk '{print $2}' | tr -d \' | sed 's/);//g' | tr -d '\r'`
    	DB_NAME=`cat ${SITES_PATH}/${SITE_NAME}/wp-config.php | grep -v "*" | grep -v "#" | grep define | grep DB_NAME | awk '{print $2}' | tr -d \' | sed 's/);//g' | tr -d '\r'`
    	DB_USER=`cat ${SITES_PATH}/${SITE_NAME}/wp-config.php | grep -v "*" | grep -v "#" | grep define | grep DB_USER | awk '{print $2}' | tr -d \' | sed 's/);//g' | tr -d '\r'`
    	DB_PASSWORD=`cat ${SITES_PATH}/${SITE_NAME}/wp-config.php | grep -v "*" | grep -v "#" | grep define | grep DB_PASSWORD | awk '{print $2}' | tr -d \' | sed 's/);//g' | tr -d '\r'`
    	mysqldump -h${DB_HOST} -u${DB_USER} -p${DB_PASSWORD} ${DB_NAME} > ${TODAY_BACKUP_PATH}/db.${SITE_NAME}.sql
    	log "Done"
     
    	log "Send sql backup to dropbox (${TODAY_DROPBOX_BACKUP_PATH}/db.${SITE_NAME}.sql)"
    	$DROPBOX_BACKUP_SCRIPT upload ${TODAY_BACKUP_PATH}/db.${SITE_NAME}.sql ${TODAY_DROPBOX_BACKUP_PATH}/
    	log "Done"
	else
	    log "wp-config file not found. Cant backup database. Skipping this step."
	fi
 
 
	log "Backuping files to ${TODAY_BACKUP_PATH}/files.${SITE_NAME}.tar.gz"
	cd ${SITES_PATH} && tar cfzh ${TODAY_BACKUP_PATH}/files.${SITE_NAME}.tar.gz ${SITE_NAME} && cd - > /dev/null
	log "Done"
 
	log "Send files backup to dropbox (${TODAY_DROPBOX_BACKUP_PATH}/files.${SITE_NAME}.tar.gz)"
	$DROPBOX_BACKUP_SCRIPT upload ${TODAY_BACKUP_PATH}/files.${SITE_NAME}.tar.gz ${TODAY_DROPBOX_BACKUP_PATH}/
	log "Done"
 
	# Clean
	log "Cleaning old backups"
	TOTAL_DIRS=`ls ${BACKUP_PATH} | wc -l`
	NB_TO_DELETE=$(( $TOTAL_DIRS - $NB_DAYS_TO_KEEP))
	if [ $NB_TO_DELETE -lt 1 ]
	then
	        log "There is no local directory to delete (there are $TOTAL_DIRS in the directory, but $NB_DAYS_TO_KEEP directories to keep)"
	else
        	for DIR_TO_DELETE in `ls ${BACKUP_PATH} | head -$NB_TO_DELETE`
        	do
        	       log "removing local directory ${BACKUP_PATH}/${DIR_TO_DELETE}"
        	       rm -r ${BACKUP_PATH}/${DIR_TO_DELETE}
        	done
	fi
 
	# dropbox cleaning
	TOTAL_REMOTE_DIRS=`$DROPBOX_BACKUP_SCRIPT list ${DROPBOX_BACKUP_PATH}/ | grep -v Listing | wc -l`
	NB_TO_DELETE=$(( $TOTAL_REMOTE_DIRS - $NB_DAYS_TO_KEEP))
	if [ $NB_TO_DELETE -lt 1 ]
	then
	        log "There is no remote directory to delete (there are $TOTAL_DIRS in the directory, but $NB_DAYS_TO_KEEP directories to keep)"
	else
        	for DIR_TO_DELETE in `$DROPBOX_BACKUP_SCRIPT list ${DROPBOX_BACKUP_PATH}/ | grep -v Listing | head -$NB_TO_DELETE | awk '{print $2}'`
        	do
        	       log "removing remote directory ${DROPBOX_BACKUP_PATH}/${DIR_TO_DELETE}"
        	       $DROPBOX_BACKUP_SCRIPT remove ${DROPBOX_BACKUP_PATH}/${DIR_TO_DELETE}
        	done
	fi
done
 
 
log "End."
